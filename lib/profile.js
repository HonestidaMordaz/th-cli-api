var http = require('http');
var printer = require('./printer');

function get (username) {
  var $_url = 'http://teamtreehouse.com/' + username + '.json';
  
  var $_request = http.get($_url, function (response) {
    var $_body = '';
    
    response.on('data', function (chunk) {
      $_body += chunk;
    });
    
    response.on('end', function () {
      if (response.statusCode == 200) {
        try {
          var $_profile = JSON.parse($_body);
          printer.printMessage($_profile);
        } catch (err) {
          printer.printError(err);
        }
      } else {
        $_errorMessage = {
          message : 'There was an error getting the profile of ' +
                      username + ' (' +
                      http.STATUS_CODES[response.statusCode] + ')'
        };
        
        printer.printError($_errorMessage);
      }
    });
  });
  
  $_request.on('error', printer.printError);
}

module.exports.get = get;