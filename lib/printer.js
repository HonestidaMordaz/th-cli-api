function printMessage (profile) {
  var $_message = profile.name + " has " +
                  profile.badges.length + " total badge(s) and " +
                  profile.points.JavaScript + " points in JavaScript";
    
  console.log($_message);
}

function printError (error) {
  console.error(error.message);
};

module.exports.printMessage = printMessage;
module.exports.printError = printError;